#!/bin/sh
#
# SPDX-License-Identifier: MPL-2.0
#
# Script that generates a static HTML video viewer.
# Requires FFmpeg.

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

videos_dir="./videos"
thumbs_dir="./thumbnails"

mkdir -p "$videos_dir"
mkdir -p "$thumbs_dir"

# Loop over all video files.
for video_file in "$videos_dir"/*; do
	video_name="$(basename "${video_file}")"
	thumb_name="$(basename "${video_file%.*}.jpeg")"
	thumb_file="$thumbs_dir/$thumb_name"

	# If thumnail doesn't exist for this video.
	if [ ! -e "$thumb_file" ]; then
		printf "generating thumbnail: %s\n" "$thumb_name"
		ffmpeg -loglevel error -i "$video_file" -frames:v 1 "$thumb_file"
	fi

	# Generate html for each item.
	items_html="$items_html""
		<div class='grid-item-container'>
			<img class='grid-item' src='${thumb_file}' data='${video_file}'/>
			<div class='player-top-bar'>
				<span class='player-top-bar-text'>$video_name</span>
			</div>
		</div>"
done

# Write index.html
echo "
<html lang='en'>
<head>
	<title>Video Viewer</title>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
	<div style='display:grid; grid-template-columns:repeat(var(--gridsize), auto)'>
		$items_html
	</div>
</body>

<script>
	document.querySelector('body').addEventListener('click', (event) => {
		const element = event.target;
		if (element.className !== 'grid-item') {
			return;
		}

		const videoPath = element.getAttribute('data');
		const videoElement = document.createElement('video');

		videoElement.className = 'grid-item'
		videoElement.setAttribute('controls', '')
		videoElement.setAttribute('autoplay', '')
		videoElement.setAttribute('data', element.src)

		const sourceElement = document.createElement('source');
		sourceElement.src = videoPath
		videoElement.appendChild(sourceElement)

		element.parentNode.replaceChild(videoElement, element);
	});
</script>

<style>
.grid-item-container {
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	width: 100%;
	max-height: 100vh;
}
.grid-item {
	width: 100%;
	height: 100%;
}
.player-top-bar {
	display: flex;
	flex-wrap: wrap;
	opacity: 0.8;
	position: absolute;
	top: 0;
	left: 0;
}
.player-top-bar-text {
	padding: 0.05em 0.4em 0.05em 0.2em;
	color:  hsl(215deg, 40%, 90%);;
	font-size: 40%;
	background: hsl(215deg, 40%, 8%);
}

:root {
	font-size: 12vw; /* Window scale */
	--gridsize: 1;
}

/* Tablet/Dektop. */
@media only screen and (min-width: 768px) {
	:root {
		font-size: 4.5vh; /* Window scale. */
		--gridsize: 3;
	}
}
</style>" > index.html
